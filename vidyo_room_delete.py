import csv
import json
import os

from common.mailer import send_email_with_deleted_rooms
from plugins.indico_api_client import IndicoAPIClient
from common.logger import setup_logging
from vidyo.vidyo_room_manager import VidyoRoomManager
from conf import CSV_FILE_PATH, DEBUG

logging = setup_logging()
logger = logging.getLogger(__name__)


if __name__ == "__main__":
    """
    MAIN
    """

    logger.info("main")
    old_rooms = VidyoRoomManager().get_rooms_older_than_a_year(limit=10)
    rooms_to_delete = ()

    sorted_rooms = sorted(old_rooms, key=lambda room: room['roomID'], reverse=False)
    for room in sorted_rooms:
        rooms_to_delete += (('rid', room['roomID']),)

    logger.debug(rooms_to_delete)

    delete_result = IndicoAPIClient().delete_rooms(rooms_to_delete)
    logger.debug(delete_result)

    logger.debug("%s rooms found", len(old_rooms))
    email_content = ''
    delete_data = json.loads(delete_result[1])

    with open(CSV_FILE_PATH, 'wb') as csv_file:
        wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
        wr.writerow(['RoomID', 'Result', 'RoomExt' 'RoomName', 'last_use', 'roomDescription'])
        for room in sorted_rooms:
            if room['roomID'] in delete_data['results']['success']:
                result = 'Deleted'
            elif room['roomID'] in delete_data['results']['failed']:
                result = 'Failed'
            else:
                result = 'Not found'
            wr.writerow([room['roomID'], result, room['roomExtNumber'], room['roomName'].encode('utf-8'), room['last_used'], room['roomDescription'].encode('utf-8')[:125]])

    if not DEBUG:
        send_email_with_deleted_rooms(CSV_FILE_PATH)
        os.remove(CSV_FILE_PATH)

