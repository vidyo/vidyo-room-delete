import logging

import sys

from conf import DEBUG


def setup_logging():
    if DEBUG:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(stream=sys.stdout, level=level)
    logging.getLogger(__name__)

    return logging
