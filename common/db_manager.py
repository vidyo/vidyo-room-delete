import MySQLdb
import pyodbc

from common.logger import setup_logging

logging = setup_logging()
logger = logging.getLogger(__name__)


class MysqlManager(object):

    def __init__(self, configuration):
        self._configuration = configuration
        self._connect()

    def _connect(self):
        """
            Create a new connection with the MySQL database
        """
        connection = None
        try:
            # connect to the database
            connection = MySQLdb.connect(**self._configuration)
        except MySQLdb.Error as err:
            logger.exception("Error while connecting to MySQL database: %s" % err)

        return connection

    def execute(self, query_string, params=None, fetch_all=False):
        """
            Execute the query appending the params at the end: this is needed to check if they are safe.
            Params can be a list, in case of multiple values for the query, or None, in case of no param
        """
        rows = []
        last_inserted_id = None
        connection = None

        try:
            if params and type(params) != type(()):
                params = tuple(params)

            connection = self._connect()
            if connection:
                cursor = connection.cursor(MySQLdb.cursors.DictCursor)

                cursor.execute(query_string, params or None)
                connection.commit()

                # get last auto increment id in case of insert query
                last_inserted_id = cursor.lastrowid

                # fetch results
                if fetch_all:
                    rows = cursor.fetchall()
                else:
                    rows = cursor.fetchone()

                cursor.close()

            else:
                logger.warning("No connection with MySQL db")

        except MySQLdb.Error as err:
            logger.exception("Error while executing the last MySQL query: %s. Rolling back" % err)
            connection.rollback()

        if connection:
            connection.close()

        return rows, last_inserted_id


class ODBC_Manager():

    def __init__(self, conn_string):
        self._conn_string = conn_string
        self._connect()

    def _connect(self,):
        """
            Create a new connection to a database using ODBC
        """
        connection = None
        try:
            # connect to the database, using a connection string for ODBC
            connection = pyodbc.connect(self._conn_string, autocommit=True, unicode_results=True)
        except pyodbc.Error as err:
            logger.exception("Error while connecting to database using ODBC: %s" % err)

        return connection

    def execute(self, query, params=None, fetch_all=False):
        """
            Execute the query appending the params at the end: this is needed to check if they are safe.
            Params can be a list, in case of multiple values for the query, or None, in case of no param
        """
        connection = None
        rows = None

        # if params is list, convert them from list to tuple
        if isinstance(params, list):
            params = tuple(params)

        try:
            connection = self._connect()
            if connection:
                cursor = connection.cursor()

                if params:
                    logger.debug("ODBC Query: %s " % (query % params))
                    cursor.execute(query, params)
                else:
                    logger.debug("ODBC Query: %s " % query)
                    cursor.execute(query)

                # fetch results
                if fetch_all:
                    rows = cursor.fetchall()
                else:
                    rows = cursor.fetchone()

                cursor.close()

            else:
                logger.warning("No connection through ODBC")

        except pyodbc.Error as err:
            logger.exception("Error while executing the last ODBC query: %s. Rolling back" % err)
            connection.rollback()

        if connection:
            connection.close()
        return rows, None
