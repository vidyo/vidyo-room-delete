import smtplib
import socket
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from conf import NOTIFICATIONS_EMAIL


def send_email_with_deleted_rooms(file_to_attach):

    from_domain = socket.getfqdn()
    from_email = "vccron@"+from_domain
    email_recipients = NOTIFICATIONS_EMAIL
    # Create an html message
    today_date = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    html = """\
    <html>
      <head></head>
      <body>
        <p>Hello, </p>
        <p>These are the vidyo rooms that were deleted """ + today_date + """ reported by """ + from_domain + """:</p>
        <p>Best regards.</p>
        <p>The automated script.</p>
       </body>
    </html>
    """
    msg = MIMEMultipart()
    msg.attach(MIMEText(html, 'html'))

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = '[VCCRON] Deleted old Vidyo rooms ' + today_date
    msg['From'] = from_email
    msg['To'] = email_recipients[0]

    f = file(file_to_attach)
    attachment = MIMEText(f.read())
    attachment.add_header('Content-Disposition', 'attachment', filename="deleted_rooms.csv")
    msg.attach(attachment)

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtplib.SMTP('localhost')
    s.sendmail(from_email, email_recipients, msg.as_string())
    s.quit()