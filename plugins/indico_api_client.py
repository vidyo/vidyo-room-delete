import hashlib
import hmac
import time
import urllib
from contextlib import closing

import requests
from werkzeug.datastructures import MultiDict

import conf as config


from common.logger import setup_logging

logging = setup_logging()
logger = logging.getLogger(__name__)


class IndicoAPIClient():
    """
    Class and methods to connect to and use Indico HTTP API.
    Check Indico docs for more info: http://indico.readthedocs.org/en/master/http_api/
    """
    API_BASE = '/api/'

    def delete_rooms(self, arg_params=None):
        """
        Deletes all the given rooms.
        :param arg_params: Expects A tuple of tuples: (('room_id', 111), ('room_id', 111),)
        :return: A tuple with 2 elements. The second one is the data string.
        """
        if arg_params is None:
            arg_params = ()
        path = self.API_BASE + 'deletevcroom/vidyo.json'
        return self._indico_request('POST', path, arg_params=arg_params)

    def _build_indico_request(self, path, params, api_key=None, secret_key=None, only_public=False):
        items = params.items() if hasattr(params, 'items') else list(params)
        if api_key:
            items.append(('apikey', api_key))
        if only_public:
            items.append(('onlypublic', 'yes'))
        if secret_key:
            items.append(('timestamp', str(int(time.time()))))
            items = sorted(items, key=lambda x: x[0].lower())
            url = '%s?%s' % (path, urllib.urlencode(items))

            signature = hmac.new(secret_key, url, hashlib.sha1).hexdigest()
            items.append(('signature', signature))
        return items

    def _indico_request(self, method, path, arg_params):
        indico_url = config.INDICO_URL + path
        request_values = MultiDict(arg_params)
        method = method.upper()
        params = request_values.items(multi=True)

        data = self._build_indico_request(path, params, config.INDICO_API_KEY, config.INDICO_API_SECRET_KEY)
        request_args = {'params': data} if method == 'GET' else {'data': data}
        try:
            logger.debug(indico_url)
            response = requests.request(method, indico_url, verify=False, **request_args)
        except requests.HTTPError as e:
            response = e.response
        except requests.ConnectionError as e:
            return 'text/plain', str(e)
        content_type = response.headers.get('Content-Type', '').split(';')[0]
        with closing(response):
            return content_type, response.text
