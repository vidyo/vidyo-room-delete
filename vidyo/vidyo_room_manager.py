import datetime

from common.db_manager import MysqlManager
from conf import VIDYO_DBCDR
import constants
from common.logger import setup_logging

logging = setup_logging()
logger = logging.getLogger(__name__)


class VidyoRoomManager(object):

    def __init__(self):
        logger.debug("Initializing VidyoRoomManager")
        self._database_connector = MysqlManager(VIDYO_DBCDR)

    def _get_rooms_older_than(self, date, limit):
        logger.debug((constants.ROOMS_LAST_USED_ON_DATE % (date, limit)))
        result, _ = self._database_connector.execute((constants.ROOMS_LAST_USED_ON_DATE % (date, limit)), fetch_all=True)
        return result

    def _get_rooms_from_user_id(self, user_id, room_name, limit):
        logger.debug((constants.ROOMS_FROM_USER % (user_id, room_name, limit)))
        result, _ = self._database_connector.execute((constants.ROOMS_FROM_USER % (user_id, room_name, limit)), fetch_all=True)
        return result

    def get_rooms_older_than_2015(self):
        """
        Get the rooms older than 2015.
        Only for testing purposes
        :return: Array with all the requested rooms
        """
        date = datetime.datetime(2015, 1, 1)
        old_rooms = self._get_rooms_older_than(date)
        return old_rooms

    def get_rooms_older_than_a_year(self, limit=100):
        """
        Get rooms older than a year.
        :return: Array with all the requested rooms
        """
        today = datetime.datetime.now()
        date = datetime.datetime(today.year - 1, today.month, today.day)
        old_rooms = self._get_rooms_older_than(date, limit)
        return old_rooms

    def get_rooms_from_user(self, user_id=None, room_name=None, limit=100):
        """
        Get rooms older than a year.
        :return: Array with all the requested rooms
        """
        # user_id = 12
        old_rooms = self._get_rooms_from_user_id(user_id, room_name, limit)
        return old_rooms
