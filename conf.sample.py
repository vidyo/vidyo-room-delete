DEBUG = True
LOGS_FOLDER = 'logs/'
NOTIFICATIONS_EMAIL = ''
CSV_FILE_PATH = ''

VIDYO_DBCDR ={
    'host': '', #vidyoportalbeta01.cern.ch
    'port': 000,
    'user': '',
    'passwd': '',
    'db': '',
    'charset': '',
    'use_unicode': True
}

INDICO_URL = ""
INDICO_API_KEY = ""
INDICO_API_SECRET_KEY = ""
